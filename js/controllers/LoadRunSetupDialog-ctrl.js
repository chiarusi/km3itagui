/**
 * Created by favaro on 20/11/15.
 */
app.controller('LoadRunSetupCtrl',
  [
    '$scope',
    '$cookieStore',
    '$log',
    'GlobalDataService',
    '$modalInstance',
    '$timeout',
    'RemoteApiFactory',
    LoadRunSetupCtrl]);

function LoadRunSetupCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  $modalInstance,
  $timeout,
  RemoteApiFactory) {
  //-- Variables --//
  console.log("LoadRunSetupCtrl controller");
  $scope.filename = "";
  $scope.content  = "";
  $scope.fileFD   = {};

  $scope.errorstring = "";
  $scope.okEnabled   = false;
  //-- Methods --//

  $scope.cancel = function () {
    $modalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      RemoteApiFactory.uploadRunSetup(token,
                                      $scope.content,
                                      $scope.fileFD,
                                      $scope.filename)
        .success(function (data) {
                   if(data.action == "ok") {
                     $modalInstance.dismiss();
                   }
                        else {
                     $scope.errorstring = data.message;
                   }
                 })
        .error(function (data, status) {
                 $log.error(data);
                 $log.error(status);
                       $modalInstance.dismiss();

               });

    }

  };

  //$modalInstance.close({content: $scope.content, filename: $scope.filename});

 // end save

$scope.hitEnter = function (evt) {
  if (angular.equals(evt.keyCode, 13) && !(angular.equals($scope.user.name,
                                                          null) || angular.equals($scope.user.name,
                                                                                  '')))
    $scope.save();
};

$scope.loadContent = function (content, filename, fileFD) {
  $scope.filename = filename;

  try {
    $scope.content     = JSON.parse(content);
    $scope.errorstring = "";
    $scope.fileFD      = fileFD;
    $scope.okEnabled   = true;
  }
  catch (e) {
    $scope.errorstring = "not a JSON FILE!!!!!";
    $scope.okEnabled   = false;
  }
}
}
