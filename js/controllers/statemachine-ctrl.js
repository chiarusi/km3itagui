/**
 * StateMachine Controller
 */
app.controller('StateMachineCtrl',
               [
                 '$scope',
                 '$cookieStore',
                 '$log',
                 'GlobalDataService',
                 'dialogs',
                 'RemoteApiFactory',
                 '$state',
                 '$interval',
                 StateMachineCtrl]);

function StateMachineCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  dialogs,
  RemoteApiFactory,
  $state,
  $interval) {

  /**
   * StateMachineCtrl Toggle & Cookie Control
   */

  var informationUpdatePromise;
  var enableElements = function () {
    $scope.disabled = GlobalDataService.getDisableElements();
    //$log.log("StateMachineCtrl-->enableElements("+$scope.isDisabled+") GlobalDataService.getDisableElements()-->"+ GlobalDataService.getDisableElements());

  };

  var enableStateButtons = function (notification) {
    //$log.log("enableStateButtons:" + notification);

    $scope.state        = GlobalDataService.getStateMachineState();
    $scope.state        = ($scope.state == "" ? 'idle' : $scope.state);
    $scope.idleState    = ($scope.state.toLowerCase() == 'idle');
    $scope.standbyState = ($scope.state.toLowerCase() == 'standby');
    $scope.readyState   = ($scope.state.toLowerCase() == 'ready');
    $scope.runningState = ($scope.state.toLowerCase() == 'running');
    $scope.processes    = GlobalDataService.getProcessStatus();
    $scope.runsetup     = GlobalDataService.getRunsetup();
    $scope.datacard     = GlobalDataService.getDatacard();
    $scope.reason       = GlobalDataService.getReason();
    $scope.runnumber    = GlobalDataService.getRunnumber();
    $scope.idleTime     = GlobalDataService.getIdleTime();
    $scope.standbyTime  = GlobalDataService.getStandbyTime();
    $scope.readyTime    = GlobalDataService.getReadyTime();
    $scope.runningTime  = GlobalDataService.getRunningTime();
    $scope.timestamp    = GlobalDataService.getStateTimestamp();
    $scope.alerts       = [];
    //$log.log("standbyState:"+$scope.standbyState);
    //$log.log("readyState:"+$scope.readyState);
    //$log.log("runningState:"+$scope.runningState);
    //$log.log("$scope.disabled:"+$scope.disabled);

  };

  var startUpdateInformation = function () {
    if (!angular.isDefined(informationUpdatePromise)) {
      $log.log("StateMachineCtrl--> start updateinformation");
      informationUpdatePromise = $interval(priviledge_expiration, 1000);
    }
    else {
      $log.log("StateMachineCtrl--> decrementer already running...");

    }

  };
  var stopUpdateInformation  = function () {
    if (angular.isDefined(informationUpdatePromise)) {
      $log.log("StateMachineCtrl--> halting decrementer...");
      $interval.cancel(informationUpdatePromise);
      informationUpdatePromise = undefined;
    }
  };

  var priviledge_expiration = function () {
    var countdown = GlobalDataService.getPriviledgeWillExpireInSeconds();

    if (GlobalDataService.getCurrentPriviledgedName().length != 0) {
      //$log.log(countdown);
      if (countdown > 0) {
        $scope.expiring_priviledge = ($scope.isPriviledged ? "Your" :
            GlobalDataService.getCurrentPriviledgedName() )
          + " priviledge will expire in "
          + Math.floor(countdown / 60) +
          ":" + ("0" + (countdown % 60)).slice(-2)
          + " mins";
      }
      else {
        $scope.expiring_priviledge = ($scope.isPriviledged ?
            "Your"
            : GlobalDataService.getCurrentPriviledgedName() ) +
          " priviledge is expired";
        $scope.isPriviledged       = false;
        stopUpdateInformation();
      }
    }
    else {
      $scope.expiring_priviledge = "";
      $log.log(
        "StateMachineCtrl--> priviledge_expiration--> io want to halt the timer");
      stopUpdateInformation();
    }
  };

  var priviledgeInformation = function (priviledge) {
    $log.log("StateMachineCtrl--> priviledgeInformation fired");
    $scope.isPriviledged = GlobalDataService.getPriviledgeStatus();
    $log.log("StateMachineCtrl-->  " + $scope.isPriviledged);
    $scope.currentPriviledged    = ($scope.isPriviledged ?
        "You are the current priviledged user" :
        (GlobalDataService.getCurrentPriviledgedName().length == 0 ?
          "No priviledged user at moment" :
        "The current Priviledged user is " + GlobalDataService.getCurrentPriviledgedName() + ".")
    );
    $scope.escalating_priviledge = (GlobalDataService.getCurrentEscalatingName().length == 0 ?
        "" :
        (GlobalDataService.getCurrentEscalatingName() == GlobalDataService.getName() ?
            " You are escalating... " :
          " User: " + GlobalDataService.getCurrentEscalatingName() + " is escalating."
        )
    );
    startUpdateInformation();
  };

  var successCommandFunction = function (result) {
    $log.log(result);
    if (result.action == 'ok') {
      //dialogs.notify("Commands sent", result.message);
      $scope.alerts.push({type: 'success', msg: result.message});
      enableStateButtons();
    }
  };
  var errorCommandFunction   = function (data, status, headers, config) {
    $log.log(status);
    $log.log(data);
    if (status >= 500) {
      $scope.alerts.push({type: 'danger', msg: result.message});
    }
    else {
      //dialogs.error(data.action[0].toUpperCase() + data.action.substring(1),
      //              data.message);
      $scope.alerts.push({type: 'danger',
                           msg: data.action[0].toUpperCase() + data.action.substring(
                             1) + ": " + data.message
                         });

    }
  };

  $scope.status = {
    isopen: false
  };

  $scope.toggled = function (open) {
    $log.log('Dropdown is now: ', open);
  };

  $scope.toggleDropdown = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

  $scope.init      = function () {
    $scope.dlg = dialogs.create('/templates/runsetupselect.html',
                                'RunSetupCtrl',
                                $scope.custom,
                                {size: 'lg'});
    $scope.dlg.result.then(function (runsetup) {

      if (angular.isDefined($cookieStore.get('token'))) {
        token = $cookieStore.get('token');
        RemoteApiFactory.initCommand(token, runsetup)
          .success(function (data) {
            $scope.alerts.push({type: 'success', msg: data.message});

            //dialogs.notify("Commands sent", data.message);
            $scope.runsetup = runsetup;
          })
          .error(errorCommandFunction);
      }
    });

  };
  $scope.configure = function () {
    $scope.standbyState = false;
    var token           = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.configureCommand(token).success(successCommandFunction).error(
      errorCommandFunction);
  };
  $scope.reset     = function () {
    $scope.standbyState = false;
    var token           = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.resetCommand(token).success(successCommandFunction).error(
      errorCommandFunction);
  };
  $scope.start     = function () {
    $scope.standbyState = false;
    var token           = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.startCommand(token).success(successCommandFunction).error(
      errorCommandFunction);
  };
  $scope.stop      = function () {
    $scope.standbyState = false;
    var token           = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.stopCommand(token).success(successCommandFunction).error(
      errorCommandFunction);
  };

  $scope.iwouldliketoescalate = function () {
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.iwouldliketoescalate(token,
                                          true).success(function (result) {
      $log.log("StateMachineCtrl-->iwouldliketoescalate:");
      $log.log(result);
      GlobalDataService.setCurrentPriviledgedName(result.currentPriviledgedName);
      GlobalDataService.setCurrentPriviledgedUsername(result.currentPriviledgedUserName);
      GlobalDataService.setCurrentEscalatingName(result.currentEscalatingName);
      GlobalDataService.setPriviledgeWillExpireInSeconds(result.priviledgeWillExpireInSeconds);
      GlobalDataService.setPriviledgeStatus(('escalationCompleted' === result.result));

      GlobalDataService.notifyPriviledgeObserver();

    }).error(function (result) {
      $log.warn(result);
    });
  };
  $scope.releasePriviledge    = function () {
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.releasePriviledge(token, true).success(function (result) {
      $log.log("StateMachineCtrl-->releasePriviledge:");
      $log.log(result);
      GlobalDataService.setCurrentPriviledgedName(result.currentPriviledgedName);
      GlobalDataService.setCurrentPriviledgedUsername(result.currentPriviledgedUserName);
      GlobalDataService.setCurrentEscalatingName(result.currentEscalatingName);
      GlobalDataService.setPriviledgeWillExpireInSeconds(result.priviledgeWillExpireInSeconds);
      GlobalDataService.setPriviledgeStatus(false);

      GlobalDataService.notifyPriviledgeObserver();

    }).error(function (result) {
      $log.log(result);
    });

  };
  $scope.closeAlert           = function (index) {
    $scope.alerts.splice(index, 1);
  };

  // subscribbing observer
  GlobalDataService.registerDisableElementsObserver(enableElements);
  GlobalDataService.registermachineStateObserver(enableStateButtons);
  GlobalDataService.registerPriviledgedObserver(priviledgeInformation);
  //isPriviledged();
  //enableElements();
  enableStateButtons();
  priviledgeInformation();
  GlobalDataService.setWebsocketConnected(GlobalDataService.getWebsocketConnected());

}

