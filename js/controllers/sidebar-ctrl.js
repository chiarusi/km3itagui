/**
 * SideBar Controller
 */
app.controller('SidebarCtrl', [
  '$scope',
  '$cookieStore',
  '$log',
  '$state',
  'GlobalDataService',
  'dialogs',
  'RemoteApiFactory',
  SidebarCtrl]);

//SideBar Controller
function SidebarCtrl(
  $scope,
  $cookieStore,
  $log,
  $state,
  GlobalDataService,
  dialogs,
  RemoteApiFactory) {
  /**
   * Sidebar Toggle & Cookie Control
   */
  var enableElements = function () {
    $scope.isDisabled = GlobalDataService.getDisableElements();
    //$log.log("SidebarCtrl-->enableElements("+$scope.isDisabled+") GlobalDataService.getDisableElements()-->"+ GlobalDataService.getDisableElements());
  };

  $scope.$log = $log;
  //$log.debug("SidebarCtrl");
  var mobileView = 992;
//alert("loaded SidebarCtrl!!!!");
//getting parameter
  //$log.debug("SidebarCtrl:"+GlobalDataService.getShowItem());

  $scope.showItem = "+++";//GlobalDataService.getShowItem();
  $scope.hideElement = false;

  //$log.debug("SidebarCtrl:"+$scope.showItem);

  $scope.goToStatemachinePage = function () {
    $log.debug("going into statemachine state");
    $state.go('State Machine');
  };
  $scope.showloadrunsetupeditor = function () {
    $log.debug("going into Runsetup Editor state");
    $state.go('Runsetups');
  };
  $scope.showloadrunsetup     = function () {
    $scope.dlg = dialogs.create('/templates/loadRunsetupFromfile.html',
                                'LoadRunSetupCtrl',
                                $scope.custom,
      {size: 'lg'});
    $scope.dlg.result.then(function (result) {
      RemoteApiFactory.uploadRunSetup
      $log.debug(result);

    });
  };

  GlobalDataService.registerDisableElementsObserver(enableElements);
  //enableElements();

}
