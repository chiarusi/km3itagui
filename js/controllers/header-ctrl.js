/**
 * Header Controller
 */
app.controller('HeaderCtrl', [
  '$scope',
  '$cookieStore',
  '$log',
  'GlobalDataService',
  'dialogs',
  '$state',
  '$filter',
  HeaderCtrl]);

//Header Controller
function HeaderCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  dialogs,
  $state,
  $filter) {
  /**
   * HeaderCtrl Toggle & Cookie Control
   */
  $scope.notifications      = [];
  $scope.websocketConnected = false;
  $scope.daemonRunning      = false;
  $scope.commandOnGoing     = false;

  if (angular.isDefined($cookieStore.get('notifications')))
    $scope.notifications = $cookieStore.get('notifications');

  var loginInformation = function () {
    $scope.name = GlobalDataService.getName();
    $scope.role = GlobalDataService.getRole();
    $log.log("HeaderCtrl-->loginInformation:" + $scope.name + ":" + $scope.role);
  };

  var enableElements     = function () {
    $scope.isDisabled = GlobalDataService.getDisableElements();
    //$log.log("HeaderCtrl-->enableElements("+$scope.isDisabled+") GlobalDataService.getDisableElements()-->"+ GlobalDataService.getDisableElements());
  };
  var notificationUpdate = function (notification) {

    if ((notification != null) &&
      (angular.isDefined(notification.data) && notification.data.statechange )) {
      $log.log(notification);
      $scope.notifications.push(notification);
      $cookieStore.put('notifications', $scope.notifications);
    }
    else {
      //$log.log("header-ctrl: notification ommited");
    }

  };

  var daemonRunningUpdate  = function (daemonrunning) {
    //$log.log("header-ctrl: daemonRunningUpdate");
    $scope.daemonRunning = daemonrunning;
    $log.log("header-ctrl: $scope.daemonRunning " + $scope.daemonRunning);

  };
  var commandOngoingUpdate = function (commandongoing) {
    //$log.log("header-ctrl: commandOngoingUpdate");
    $scope.commandOnGoing = commandongoing;
    $log.log("header-ctrl: $scope.commandOnGoing " + $scope.commandOnGoing);
  };

  var webSocketConnectedUpdate = function (wesocketConnected) {
    $log.log("header-ctrl: webSocketConnectedUpdate");

    $scope.websocketConnected = wesocketConnected;
  };

  $scope.openViewfromNotification = function (notificationIndex) {
    //$log.log("openViewfromNotification("+notificationIndex+")");
    var clicked = $scope.notifications.splice(notificationIndex, 1)[0];
    //$log.log($filter('notification'));
    //$scope.notifications = $filter('notification')($scope.notifications, clicked);
    /*$scope.notifications.filter(function(element) {
     return this.view != element.view;
     },clicked);
     * */
    //$cookieStore.put('notifications', $scope.notifications);
    $state.go(clicked.view);
  };

  $scope.clearNotification = function () {
    $scope.notifications.length = 0;
    $cookieStore.put('notifications', $scope.notifications);
  };
  $scope.logout            = function () {
    GlobalDataService.setlogOut();
  };
  $scope.$log              = $log;

  var mobileView = 992;

  $scope.pageName      = $state.current.name;
  $scope.pagePath      = "Home" + $state.current.url;
  $scope.notifications = $filter('notification')($scope.notifications,
    {view: $state.current.name});
  $cookieStore.put('notifications', $scope.notifications);

  GlobalDataService.registerLoginObserver(loginInformation);
  GlobalDataService.registerDisableElementsObserver(enableElements);

  GlobalDataService.registermachineStateObserver(notificationUpdate);
  GlobalDataService.registerPriviledgedObserver(notificationUpdate);
  GlobalDataService.registerDaemonRunningObserver(daemonRunningUpdate);
  GlobalDataService.registerCommandOngoingObserver(commandOngoingUpdate);
  GlobalDataService.registerWebSocketConnectedObserver(webSocketConnectedUpdate);

  //enableElements();
}
