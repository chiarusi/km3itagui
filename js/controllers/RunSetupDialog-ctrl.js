/**
 * Created by favaro on 20/11/15.
 */
app.controller('RunSetupCtrl',
  [
    '$scope',
    '$cookieStore',
    '$log',
    'GlobalDataService',
    '$modalInstance',
    '$timeout',
    'RemoteApiFactory',
    RunSetupCtrl]);

function RunSetupCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  $modalInstance,
  $timeout,
  RemoteApiFactory) {
  //-- Variables --//
  console.log("RunSetupCtrl controller");
  $scope.runsetup      = {};
  $scope.runsetuplists = {};

  //-- Methods --//

  $scope.cancel = function () {
    $modalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $modalInstance.close($scope.runsetup.selected);

  }; // end save

  $scope.hitEnter = function (evt) {
    if (angular.equals(evt.keyCode, 13) && !(angular.equals($scope.user.name,
                                                            null) || angular.equals($scope.user.name,
                                                                                    '')))
      $scope.save();
  };

  if (angular.isDefined($cookieStore.get('token'))) {
    token = $cookieStore.get('token');
    RemoteApiFactory.getPublishedRunSetupListCommand(token)
      .success(function (data, status) {
                 $log.debug("RunSetupCtrl get runsetuplist");
                 $log.debug(data);
                 $scope.runsetuplist = data.result;
                 $log.debug($scope.runsetuplist);

               })
      .error(function (data, status) {
        $log.error(data);
        $log.error(status);
             });
  } else {
    GlobalDataService.setPriviledgeStatus(false);
    $modalInstance.dismiss('Canceled');

  }
}
