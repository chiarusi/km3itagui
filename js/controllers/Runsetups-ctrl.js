/**
 * Created by favaro on 25/07/16.
 */
app.controller('RunsetupsCtrl',
               [
                 '$scope',
                 '$cookieStore',
                 '$log',
                 'GlobalDataService',
                 'dialogs',
                 'RemoteApiFactory',
                 '$state',
                 '$interval',
                 RunsetupsCtrl
                 ]);

function RunsetupsCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  dialogs,
  RemoteApiFactory,
  $state,
  $interval) {

  $scope.loadDraftRunsetup = function () {

    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);

      RemoteApiFactory.getRunsetupDraftListCommand(token)
        .success(function (data) {
          $log.debug(data);
          $scope.runsetupDraftList = data.result;

        })
        .error(function (data, errorCode) {
          $log.error(data);
          $log.error(errorCode);
          if (errorCode == 403) {
            GlobalDataService.setlogOut();
            $state.go('Runsetups');
          }
        });
    }
  };

  $scope.openPublishedRunSetupList = function () {
    $scope.dlg = dialogs.create('/templates/runsetupselect.html',
                                'RunSetupCtrl',
                                $scope.custom,
                                {size: 'lg'});
    $scope.dlg.result.then(function (runsetup) {
      $log.debug("runsetup to display:");
      $log.debug(runsetup);
      $state.go('Runsetup Editor', {
        'runsetup': runsetup
        });
      });
    };
  
  $scope.newRunsetup = function () {
    $scope.dlg = dialogs.create('/templates/detectorSelect.html',
                                'DetectorSelectCtrl',
                                $scope.custom,
                                {size: 'lg'});
    $scope.dlg.result.then(function (detector) {
      console.log("detectorId:"+detector);
      console.log(detector);
      $state.go('Runsetup Editor', {
        runsetup: {
          detector_id: detector.id,
          state: "new"
        }
      });

    });
  };
  $scope.upLoadRunSetup = function() {
    $scope.dlg = dialogs.create('/templates/loadRunsetupFromfile.html',
                              'LoadRunSetupCtrl',
                              $scope.custom,
                              {size: 'lg'});
    $scope.dlg.result.then(function (result) {
      $log.debug(result);
    });
  };

  $scope.editRunsetup = function(runsetup) {
    $log.debug("runsetup to display:");
    $log.debug(runsetup);
    $state.go('Runsetup Editor', {
      'runsetup': runsetup
    });
  };

  $scope.deleteRunsetup = function(runsetup) {
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);
      if (runsetup.state == "draft") {
        RemoteApiFactory.deleteDraftRunsetup(token,
                                        runsetup.id).success(
          function (data) {
            $log.debug(data);
            $scope.loadDraftRunsetup();
          });
      }
    }

  };
  $scope.publishRunsetup = function(runsetup) {
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);
      if (ctrl.runsetup.state == "published") {
        RemoteApiFactory.publishDraftRunsetup(token,
                                             runsetup.id).success(
          function (data) {
            $log.debug(data);
            $scope.loadDraftRunsetup();
          });
      }
    }

  };

  $scope.cloneRunsetup= function(runsetup) {
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);
        RemoteApiFactory.cloneDraftRunsetup(token,
                                              runsetup.id).success(
          function (data) {
            $log.debug(data);
            $scope.loadDraftRunsetup();
          });

    }

  };


  $scope.changeDetector = function(runsetup) {
    $scope.dlg = dialogs.create('/templates/detectorSelect.html',
                                'DetectorSelectCtrl',
                                {
                                  id: runsetup.detector_id
                                },
                                {size: 'lg'});
    $scope.dlg.result.then(function (detector) {
      console.log("detectorid:"+detector);
      console.log(detector);
      runsetup.detector_id = detector.id;
      if (angular.isDefined($cookieStore.get('token'))) {
        token = $cookieStore.get('token');
        $log.debug("token:" + token);
          RemoteApiFactory.updateRunsetupDetector(token,
                                                runsetup.id,
                                                  detector.id).success(
            function (data) {
              $log.debug(data);
              $scope.loadDraftRunsetup();
            });
        }
    });
  };

  //GlobalDataService.setWebsocketConnected(GlobalDataService.getWebsocketConnected());
    $scope.loadDraftRunsetup();
}