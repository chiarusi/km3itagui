/**
 * Created by favaro on 20/11/15.
 */
app.controller('DetectorSelectCtrl',
               [
                 '$scope',
                 '$cookieStore',
                 '$log',
                 'GlobalDataService',
                 '$modalInstance',
                 '$timeout',
                 'RemoteApiFactory',
                 'data',
                 DetectorSelectCtrl]);

function DetectorSelectCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  $modalInstance,
  $timeout,
  RemoteApiFactory,
  data) {
  //-- Variables --//
  console.log("DetectorSelectCtrl controller");
  $scope.detector      = {};
  $scope.detectorlist = [];

  //-- Methods --//

  $scope.cancel = function () {
    $modalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    console.debug($scope.detector.selected);
    $modalInstance.close($scope.detector.selected);

  }; // end save

  $scope.hitEnter = function (evt) {
    if (angular.equals(evt.keyCode, 13) && !(angular.equals($scope.user.name,
                                                            null) || angular.equals($scope.user.name,
                                                                                    '')))
      $scope.save();
  };

  if (angular.isDefined($cookieStore.get('token'))) {
    token = $cookieStore.get('token');
    RemoteApiFactory.getDetectorListCommand(token)
      .success(function (data, status) {
        $log.debug("DetectorSelectCtrl get detectorlist");
        $log.debug(data);
        $scope.detectorlist = data.result;
        $log.debug($scope.detectorlist);

        try{
          $scope.detector.selected = $scope.detectorlist.filter(function(elem){
            if(elem.id == data.id) return true;
          })[0];
        }
        catch(err) {

        }

      })
      .error(function (data, status) {
      });
  } else {
    GlobalDataService.setPriviledgeStatus(false);
    $scope.cancel();
  }
}
/**
 * Created by favaro on 22/09/16.
 */
