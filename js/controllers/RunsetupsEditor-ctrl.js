/**
 * Created by favaro on 25/07/16.
 */
app.controller('RunsetupEditorCtrl',
               [
                 '$scope',
                 '$cookieStore',
                 '$log',
                 'GlobalDataService',
                 'dialogs',
                 'RemoteApiFactory',
                 '$state',
                 '$stateParams',
                 '$interval',
                 '$http',
                 '$q',
                 RunsetupEditorCtrl
                 ]);

function RunsetupEditorCtrl(

  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  dialogs,
  RemoteApiFactory,
  $state,
  $stateParams,
  $interval,
  $http,
  $q) {
  
  var ctrl = this;

  $log.debug($stateParams);
  ctrl.runsetup = $stateParams.runsetup;
  try {
    ctrl.disableEditor=(ctrl.runsetup.state=="published");
  }
  catch (e) {
    ctrl.disableEditor = true;
  }
  if(!ctrl.runsetup) {
    $log.debug("andiamo");
    $state.go('Runsetups');
  }
  else {
    $log.debug("todo bien");
    $log.debug("ok ci sono:RunsetupEditorCtrl");
    var mypromise = $q.defer();
    ctrl.mySchema = $http.get('schema.json');

    ctrl.myStartVal = mypromise.promise;
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);
      if(ctrl.runsetup.state == "published") {
        RemoteApiFactory.getPublishedRunSetupJson(token,
                                                  ctrl.runsetup.id).success(
          function (data) {
            $log.debug(data);
            mypromise.resolve(data.result);
          });
      }
      else if(ctrl.runsetup.state == "draft") {
        RemoteApiFactory.getDraftRunSetupJson(token,
                                                  ctrl.runsetup.id).success(
          function (data) {
            $log.debug(data);
            mypromise.resolve(data.result);
          });
      }
      else {
        mypromise.resolve([]);
      }
    }
    else {
      mypromise.reject();
      GlobalDataService.setlogOut();
    }
  }

  $scope.saveRunSetup = function (data) {

    $log.debug(data);
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);
      if(ctrl.runsetup.state =='draft') {
      RemoteApiFactory.updateExistingDraftRunsetup(token,
                                      ctrl.runsetup.id,
                                      data, ctrl.runsetup.detector_id,
                                      ctrl.runsetup.description)
      .success(function (data) {
          $log.debug(data);
          $state.go('Runsetups');
        })
        .error(function(data) {
          $log.error(data);
          $state.go('Runsetups');
        });
      } else {
        RemoteApiFactory.saveNewRunsetup(token,
                                      ctrl.runsetup.description,
                                      data,
                                      ctrl.runsetup.detector_id)
          .success(function (data) {
            $log.debug(data);
            $state.go('Runsetups');
          })
          .error(function(data) {
            $log.error(data);
            $state.go('Runsetups');
          });
      }
    }
    $log.debug(ctrl.runsetup);
    

  };
  $scope.changeDescription = function () {
    $scope.dlg = dialogs.create('/templates/RunsetupDescriptionEditor.html',
                                'RunsetupDescriptionEditorCtrl',
                                {
                                  description: ctrl.runsetup.description
                                },
                                {size: 'lg'});
    $scope.dlg.result.then(function (description) {
      console.log("description:"+description);
      ctrl.runsetup.description = description;
    });
  };
  $scope.openDetectorList = function () {
    $scope.dlg = dialogs.create('/templates/detectorSelect.html',
                                'DetectorSelect-ctrl',
                                $scope.custom,
                                {size: 'lg'});
    $scope.dlg.result.then(function (detector) {
      console.log("detector:"+detector);
    });
  };
  $scope.moveToDraft = function () {
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      $log.debug("token:" + token);

      RemoteApiFactory.moveRunsetupToDraftCommand(token,
                                       ctrl.runsetup.id)
        .success(function (data) {
          $log.debug(data);
          $state.go('Runsetups');
        })
        .error(function(data) {
          $log.error(data);
          $state.go('Runsetups');
        });
    }
  };





//buttons-controller="SchemaCtrl"
}