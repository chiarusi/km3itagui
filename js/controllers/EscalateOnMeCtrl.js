/**
 * Created by favaro on 14/06/16.
 */
/**
 * Login Controller
 */

app.controller('EscalateOnMeCtrl', [
  '$scope',
  '$cookieStore',
  '$log',
  'GlobalDataService',
  '$modalInstance',
  'RemoteApiFactory',
  'data',
  'dialogs',
  '$timeout',
  '$interval',
  '$rootScope',
  EscalateOnMeCtrl]);

function EscalateOnMeCtrl(
  $scope,
  $cookieStore,
  $log,
  GlobalDataService,
  $modalInstance,
  RemoteApiFactory,
  data,
  dialogs,
  $timeout,
  $interval,
  $rootScope) {

  var decreaseAutograntTimeoutPromise;


  $scope.secondToPretty = function (seconds) {
    var minutes = Math.floor(seconds / 60);

    var secondsf = seconds - minutes * 60;
    //$log.log("MasterCtrl--> decrementing " + seconds);
    return $scope.str_pad_left(minutes,
                               '0',
                               2) + ':' + $scope.str_pad_left(secondsf,
                                                              '0',
                                                              2);

  };

  var decreaseAutograntTime = function () {
    var seconds = GlobalDataService.getAutograntWillExpireInSeconds();
    if (seconds > 0) {

      GlobalDataService.setAutograntWillExpireInSeconds(--seconds);

      $scope.secondToAutogrant = $scope.secondToPretty(seconds);

      GlobalDataService.getAutograntWillExpireInSeconds();
      if ($scope.secondsToForceEnabling > 0)
        --$scope.secondsToForceEnabling;
    }
    else {
      GlobalDataService.setAutograntWillExpireInSeconds(0);
      stopAutograntingTimer();
    }
  };

  var startAutograntingTimer = function () {
    if (!angular.isDefined(decreaseAutograntTimeoutPromise)) {
      $log.log("MasterCtrl--> start priviledge time decrementer");
      decreaseAutograntTimeoutPromise = $interval(decreaseAutograntTime, 1000);
    }
    else {
      $log.log("MasterCtrl--> decrementer already running...");

    }

  };

  var stopAutograntingTimer = function () {
    if (angular.isDefined(decreaseAutograntTimeoutPromise)) {
      $log.log("MasterCtrl--> halting decrementer...");
      $interval.cancel(decreaseAutograntTimeoutPromise);
      decreaseAutograntTimeoutPromise = undefined;
      $modalInstance.close();
    }
  };

  $scope.yesAnswer = function () {
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
    }
    RemoteApiFactory.releasePriviledge(token, true).success(function (result) {
      $log.log("StateMachineCtrl-->releasePriviledge:");
      $log.log(result);
      $modalInstance.close();
    }).error(function (result) {

      $log.log(result);
      $modalInstance.close();
    });


  };

  $scope.noAnswer = function () {
    $scope.confirmed = 'You confirmed "No."';
    $log.info("no");

  };

  $scope.str_pad_left = function (string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
  };

  $scope.secondToAutogrant      = "";
  $scope.secondToAutogrant      = $scope.secondToPretty(GlobalDataService.getAutograntWillExpireInSeconds());
  $scope.currentEscalatingName  = data.currentEscalatingName;
  $scope.secondsToForceEnabling = data.secondsToForceEnabling;
  startAutograntingTimer();

}
