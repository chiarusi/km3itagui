/**
 * Login Controller
 */

app.controller('LoginCtrl', ['$scope',
                              '$cookieStore',
                              '$log', 
                              'GlobalDataService', 
                              '$modalInstance', 
                              'RemoteApiFactory',
                              'dialogs',
                              'focus',
                              '$timeout', 
                              '$rootScope', 
                              LoginCtrl]);
                              
function LoginCtrl($scope, 
                  $cookieStore, 
                  $log, 
                  GlobalDataService, 
                  $modalInstance, 
                  RemoteApiFactory, 
                  dialogs, 
                  focus, 
                  $timeout,
                  $rootScope) {
  //-- Variables --//
  GlobalDataService.setDisableElements(true);

  $log.log("LoginCtrl:removing token");
  $cookieStore.remove('token');

  var defaultForm = {
            username : "",
            password : ""
        };
  $scope.user = defaultForm;
  //-- Methods --//
  $scope.resetfields = function(){
    $scope.loginForm.$setPristine();
    $scope.user = null;
    $scope.focusUsername();
  }; // end resetfields
  
  $scope.focusPass = function () {
    focus('password');
  };
  $scope.focusUsername = function () {
    focus('username');
  };
  $scope.login = function(user){
    var formData = angular.copy(user);
    $scope.user.password = null;
    RemoteApiFactory.login(formData).
      success(
        function (result) {
          $log.log(result);
          $scope.remoteResult = result;
          $cookieStore.put('token', result.token)
          $modalInstance.close();
          $scope.$destroy();
          /*
          if (angular.isDefined($cookieStore.get('token'))) {
              $log.log($cookieStore.get('token'));
          }
          * */
          GlobalDataService.setResultFromLogin(result);
          GlobalDataService.setDisableElements(false);
          
        }
      ).
      error(
        function(data, status, headers, config) {
          $log.log(status);
          $log.log(data);
          if(status >= 500) {
            $scope.error("ERROR", "an error is occurred server side please contact the sysadmin --> "+status);
          }
          else {
            $scope.error(data.action[0].toUpperCase() + data.action.substring(1), data.message);
          }
        }
      );
    

  //$modalInstance.close($scope.user.name);
  }; // end login

  $scope.hitEnter = function(evt){
    if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.user.username, null) || angular.equals($scope.user.username,'')))
      $scope.login($scope.user);
  };
    
  $scope.error = function (title, message) {
      var modaloption = {
                  keyboard: true,
                };
      var dlg = dialogs.error( title, message, modaloption);
      $log.log(dlg);
      dlg.result.finally(function(btn){
                $timeout(function() {
                  $scope.focusPass();
                },100);
              });
      }
  $scope.focusUsername();
  $log.log("LoginCtrl:username focused");

}
