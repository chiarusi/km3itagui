/**
 * MasterCtrl Controller
 */

app.controller('MasterCtrl', [
  '$scope',
  '$cookieStore',
  'GlobalDataService',
  'dialogs',
  '$timeout',
  '$rootScope',
  'RemoteApiFactory',
  '$log',
  '$wamp',
  '$interval',
  MasterCtrl]);

function MasterCtrl(
  $scope,
  $cookieStore,
  GlobalDataService,
  dialogs,
  $timeout,
  $rootScope,
  RemoteApiFactory,
  $log,
  $wamp,
  $interval) {
  /**
   * MasterCtrl: Sidebar Toggle & Cookie Control & login
   */
  var mobileView         = 992;
  var _progress          = 0;
  var decreaseTimeoutPromise;
  var decreaseAutograntTimeoutPromise;
  var reconnectTimerPromise;
  $scope.reconnectActive = true;
  var enableElements     = function () {
    $scope.isDisabled = GlobalDataService.getDisableElements();
    //$log.log("MasterCtrl-->enableElements("+$scope.isDisabled+") GlobalDataService.getDisableElements()-->"+ GlobalDataService.getDisableElements());
  };

  $scope.getWidth = function () {
    return window.innerWidth;
  };

  $scope.$watch($scope.getWidth, function (newValue, oldValue) {
    if (newValue >= mobileView) {
      if (angular.isDefined($cookieStore.get('toggle'))) {
        $scope.toggle = !$cookieStore.get('toggle') ? false : true;
      } else {
        $scope.toggle = true;
      }
    } else {
      $scope.toggle = false;
    }

  });

  $scope.toggleSidebar = function () {
    $scope.toggle = !$scope.toggle;
    $cookieStore.put('toggle', $scope.toggle);
  };

  window.onresize = function () {
    $scope.$apply();
  };
  //TEST
  //passing parameter
  //GlobalDataService.setShowItem("matteo");
  //END TESTS

  //Scope Functions
  $scope.showModal = function () {
    var dlg = dialogs.confirm("test", "masterController", true);
    $log.log(dlg);
    dlg.result.then(function (btn) {
                      $scope.confirmed = 'You confirmed "Yes."';
                      console.log($scope.confirmed);

                    },
                    function (btn) {
                      $scope.confirmed = 'You confirmed "No."';
                      $log.log($scope.confirmed);
                    });
  };
  $scope.showLogin = function () {
    var dlg = dialogs.create('/templates/login.html',
                             'LoginCtrl',
                             $scope.custom,
      {size: 'lg'});
  };

  // private function
  var decreasePriviledgeTime = function () {
    var seconds = GlobalDataService.getPriviledgeWillExpireInSeconds();
    if (seconds > 0) {

      GlobalDataService.setPriviledgeWillExpireInSeconds(--seconds);
      //$log.log("MasterCtrl--> decrementing " + seconds);

      if (seconds > 0 && seconds % 60 == 0)
        recalibrateSeconds();

    }
    else {
      GlobalDataService.setPriviledgeWillExpireInSeconds(0);
      stopDecreaseTimer();
      checkPriviledge();
    }
  };
  
  //timing syncrho
  var recalibrateSeconds = function () {
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token');
      RemoteApiFactory.amIPriviledged(token).
        success(
        function (result) {
          $log.log("MasterCtrl--> recalibrateSeconds result");
          $log.log(result);
          GlobalDataService.setPriviledgeWillExpireInSeconds(result.priviledgeWillExpireInSeconds);
        }
      ).
        error(
        function (data, status, headers, config) {
          $log.log(status);
          $log.log(data);
        }
      );
    }
  }

  var startDecreaseTimer = function () {
    if (!angular.isDefined(decreaseTimeoutPromise)) {
      $log.log("MasterCtrl--> start priviledge time decrementer");
      decreaseTimeoutPromise = $interval(decreasePriviledgeTime, 1000);
    }
    else {
      $log.log("MasterCtrl--> decrementer already running...");

    }

  };

 
  var stopDecreaseTimer  = function () {
    if (angular.isDefined(decreaseTimeoutPromise)) {
      $log.log("MasterCtrl--> halting decrementer...");
      $interval.cancel(decreaseTimeoutPromise);
      decreaseTimeoutPromise = undefined;
    }
  };

  var observerPriviledgedTimer = function () {
    if (GlobalDataService.getPriviledgeWillExpireInSeconds() != 0)
      startDecreaseTimer();
    else
      $log.log("MasterCtrl: from observer i won't start a new timer useless");
  };

  var startWebsocketReconnectTrial = function () {
    $log.log("MasterCtrl: startWebsocketReconnectTrial function");

    if (angular.isDefined(reconnectTimerPromise)) {
      $log.log("MasterCtrl: cancelling timer");
      $timeout.cancel(reconnectTimerPromise);
    }
      $log.log("MasterCtrl: starting reconnect trial");
      reconnectTimerPromise = $timeout($wamp.open, 10000);
  };

  var checkExistingSession = function () {
    _progress = 50;
    $rootScope.$broadcast('dialogs.wait.progress', {'progress': _progress});
    $timeout(function () {
      $log.log($cookieStore.get('token'));
      if (angular.isDefined($cookieStore.get('token'))) {
        var token = $cookieStore.get('token');
        $log.log("MasterCtrl: token found: " + token + " checking");
        RemoteApiFactory.checkToken(token).
          success(
          function (result) {
            $log.log(result);
            $scope.remoteResult = result;
            GlobalDataService.setResultFromLogin(result);
            GlobalDataService.setDisableElements(false);
            _progress += 50;
            $rootScope.$broadcast('dialogs.wait.progress',
              {'progress': _progress});
            $timeout(function () {
              $rootScope.$broadcast('dialogs.wait.complete');
            }, 200);

          }
        ).
          error(
          function (data, status, headers, config) {
            $log.log(status);
            $log.log(data);
            $cookieStore.remove('token');
            var modaloption = {
              keyboard: true
            };
            var title;
            var message;
            _progress += 50;
            $rootScope.$broadcast('dialogs.wait.progress',
              {'progress': _progress});

            if (status >= 500) {
              title   = "ERROR";
              message = "an error is occurred server side please contact the sysadmin: " + status;
            }
            else {
              title   = data.action[0].toUpperCase() + data.action.substring(1);
              message = data.message;
            }

            $timeout(function () {
              var dlg = dialogs.error(title, message, modaloption);
              dlg.result.finally(function (btn) {
                $rootScope.$broadcast('dialogs.wait.complete');
                $scope.showLogin();
              });
            }, 2500);
          }
        );
      }
      else {
        $rootScope.$broadcast('dialogs.wait.complete');
        $scope.showLogin();
      }
    }, 500);

  };
  var checkPriviledge      = function () {
    //$log.log("MasterCtrl-->checkPriviledge fired");
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token')
      RemoteApiFactory.amIPriviledged(token).
        success(
        function (result) {
          $log.log("MasterCtrl--> checkPriviledge result");
          $log.log(result);
          stopDecreaseTimer();

          GlobalDataService.setCurrentPriviledgedName(result.currentPriviledgedName);
          GlobalDataService.setCurrentPriviledgedUsername(result.currentPriviledgedUserName);
          GlobalDataService.setCurrentEscalatingName(result.currentEscalatingName);
          GlobalDataService.setPriviledgeWillExpireInSeconds(result.priviledgeWillExpireInSeconds);

          if (result.priviledgeWillExpireInSeconds != "0") {
            $log.log("there is something to countdown");
            startDecreaseTimer();
          }
          else {
            $log.log("why do i need to start a countdown timer?");
          }

          GlobalDataService.setPriviledgeStatus(('true' === result.result));
          //if (!(result.currentPriviledgedUserName == "")) {
            GlobalDataService.notifyPriviledgeObserver();
          //}
        }
      ).
        error(
        function (data, status, headers, config) {
          $log.log(status);
          $log.log(data);
          GlobalDataService.setPriviledgeStatus(false);
        }
      );
    }
    else
      GlobalDataService.setPriviledgeStatus(false);
  };

  var checkStateMachineStatus = function () {
    $log.log("MasterCtrl-->checkStatus fired");
    var token = "";
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token')
      RemoteApiFactory.statusCommand(token)
        .success(function (result) {
                   $log.log("MasterCtrl--> checkStateMachineStatus result");
                   $log.log(result);
                   if (result.action == "ok") {
                     deamonStatusChange(Array(result.result));
                   }
                   else {

                     $timeout(function () {
                       $log.log("MasterCtrl--> checkStateMachineStatus retrying for getting status");

                       checkStateMachineStatus()
                     }, 30000);
                   }
                 })
        .error(function (data, status) {
                 $log.log("MasterCtrl-->checkStatus error");

                 $log.error(data);
                 $log.error(status);
               }
      );
    }
  };

  var priviledgeChange = function (args) {
    $log.info("MasterCtrl-->priviledgeChange event from web socket fired");
    $log.log(args);
    if (args[0].currentPriviledgedUserName != GlobalDataService.getUsername()) {
      GlobalDataService.setCurrentPriviledgedName(args[0].currentPriviledgedName);
      GlobalDataService.setCurrentPriviledgedUsername(args[0].currentPriviledgedUserName);
      GlobalDataService.setCurrentEscalatingName(args[0].currentEscalatingName);
      GlobalDataService.setPriviledgeWillExpireInSeconds(args[0].priviledgeWillExpireInSeconds);
      if(GlobalDataService.getPriviledgeStatus()) {
        GlobalDataService.setPriviledgeStatus(false);
      }
      GlobalDataService.notifyPriviledgeObserver();
    }
    else {
      if(!GlobalDataService.getPriviledgeStatus()) {
        $log.info("MasterCtrl-->priviledgeChange maybe someone gave me the privilege");
        checkPriviledge();
      }
      else {
        $log.info(
          "MasterCtrl-->priviledgeChange doing nothing... because i'm priviledged");
      }
    }

  };

  var escalationEvent  = function (args) {
    $log.info("MasterCtrl-->escalationEvent fired on private channel");
    $log.info(args[0]);
    GlobalDataService.setAutograntWillExpireInSeconds(args[0].secondsToAutoGrant);

    var dlg = dialogs.create('templates/EscalatingModal.html',
                             'EscalateOnMeCtrl', {
                               currentEscalatingName: args[0].currentEscalatingName,
                               secondsToForceEnabling: args[0].secondsToForceEnabling
                             }
                             ,
                             {size: 'md'
                             });


    dlg.result.then(function(btn){


    });

  };

  var deamonStatusChange = function (args) {
    //$log.info("MasterCtrl-->deamonStatusChange fired");
    //$log.log(args);
    if (angular.isObject(args)) {
      //$log.log(args[0]);
      if (args[0].daemonActive == "true") {
        GlobalDataService.setDaemonStatus(args[0]);
      }
      else {
        //disable state machine
      }

    }
  };
  //service data setting

  //End scope functions

  var websocketConnect = function () {
    ////// WEB SOCKET
    $log.log('MasterCtrl-->websocketConnect fired!');

    $scope.$on("$wamp.open", function (event, session) {
      $log.log('MasterCtrl-->$wamp.open: We are connected to the WAMP Router!');
      if (angular.isDefined(reconnectTimerPromise)) {
        $log.log("MasterCtrl--> deactivating reconnect timer");
        $timeout.cancel(reconnectTimerPromise);
      }
      GlobalDataService.setWebsocketConnected(true);
      $log.info("MasterCtrl--> setWebsocketConnected");
    });

    $scope.$on("$wamp.close", function (event, data) {
      $scope.reason  = data.reason;
      $scope.details = data.details;
      $log.warn("'MasterCtrl-->$wamp.close " + data.reason + ": " + data.details);
      $log.warn({"MasterCtrl": "$wamp.close ", data: data});

      //var alert = dialogs.error("Attention","I can't reach the web socket server some features will be disabled");
      GlobalDataService.setWebsocketConnected(false);
      if(!$wamp.isRetrying) {
        startWebsocketReconnectTrial();
      }
      else {
        $log.warn({"MasterCtrl": "$wamp.close -- > automatic reconnection from library"});
      }

    });
    //Wamp connection
    $wamp.open();

    // subscribe to topic

    //1° service for the priviledge and escalating
    $wamp.subscribe('com.tridas.statemachine.priviledged.change',
                    priviledgeChange);

    if (angular.isDefined($cookieStore.get('token'))) {
      token               = $cookieStore.get('token');
      wampEscalationTopic = 'com.tridas.statemachine.escalation.' + token;
      $log.log("connecting to:" + wampEscalationTopic);
      //2° service for direct message event
      $wamp.subscribe(wampEscalationTopic, escalationEvent);
    }
    else {
      $log.error("token not found.... can't connect to web socket with token");
    }

    //3° service tridas daemon status and state machine change
    $wamp.subscribe('com.tridas.daemon.state', deamonStatusChange);

  };
  var logout           = function () {

    $log.log("MasterCtrl: logout!!");

    GlobalDataService.setDisableElements(true);
    $log.log("MasterCtrl: graphic disabled!!");

    $log.log("MasterCtrl:" + $wamp);
    try {
      $wamp.close();
    }
    catch (e) {
      $log.warn(e);
    }
    $log.log("MasterCtrl: wamp closed!!");
    stopDecreaseTimer();

    //ShowLogin
    if (angular.isDefined($cookieStore.get('token'))) {
      token = $cookieStore.get('token')
      RemoteApiFactory.logout(token)
        .success(
        function (result) {
          $log.log("MasterCtrl: logout success");

          $log.log(result);
        })
        .error(
        function (data, status, headers, config) {
          $log.log("MasterCtrl: logout failed");

          $log.log(status);
          $log.log(data);
        }).finally(function () {
                     $log.log("MasterCtrl: now showing login");
                     $scope.showLogin();
                   });
    }

  };
  //subscribing observer
  //GlobalDataService.registerLoginObserver(enableElements);
  GlobalDataService.registerLoginObserver(checkPriviledge);
  GlobalDataService.registerLoginObserver(websocketConnect);
  GlobalDataService.registerLoginObserver(checkStateMachineStatus);

  GlobalDataService.registerDisableElementsObserver(enableElements);
  GlobalDataService.registerLogOutObserver(logout);
  GlobalDataService.registerPriviledgedObserver(observerPriviledgedTimer);

  //enableElements();
  var dlg = dialogs.wait('Checking Your session',
                         'I\'m checking for your previous session...',
                         _progress);

  checkExistingSession();
  //when the controller will load it starts 1 timer for apriviledge checking only for a countdown syncrho purpose
  //after if the priviledge will load a information into expireinseconds will be started a new timer for timing decreasing
  //if the priviledge change from another controller and it isn't started it will be started





}
