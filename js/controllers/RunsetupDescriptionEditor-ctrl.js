/**
 * Created by favaro on 20/11/15.
 */
app.controller('RunsetupDescriptionEditorCtrl',
               [
                 '$scope',
                 '$log',
                 '$modalInstance',
                 'data',
                 RunsetupDescriptionEditorCtrl]);

function RunsetupDescriptionEditorCtrl(
  $scope,
  $log,
  $modalInstance,
  data) {
  //-- Variables --//
  console.log("RunsetupDescriptionEditor-ctrl controller");
  console.log(data);

  //-- Methods --//
  $scope.text = data.description;
  $scope.cancel = function () {
    $modalInstance.dismiss('Canceled');
  }; // end cancel

  $scope.save = function () {
    $modalInstance.close($scope.text);

  }; // end save

  $scope.hitEnter = function (evt) {
    if (angular.equals(evt.keyCode, 13) && !(angular.equals($scope.user.name,
                                                            null) || angular.equals($scope.user.name,
                                                                                    '')))
      $scope.save();
  };

}
