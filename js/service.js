app.service('GlobalDataService', [
  '$log', function ($log) {

    var disableElements = true;
    var role; //viewer, expert, admin
    var username;
    var name;
    //var currentview;

    var stateMachineState  = "";
    var daemonStatus       = {};
    var commandOnGoing     = false;
    var websocketConnected = false;

    var priviledged                = false;
    var isLogged                   = false;
    var currentPriviledgedName     = "";
    var currentPriviledgedUsername = "";
    var currentEscalatingName      = "";
    var priviledgeWillExpire       = 0;
    var autograntWillExpire       = 0;

    var showToViewer;
    var showToExpert;
    var showToRoot;

    //Observers
    var updateCurrentviewObserverCallbacks  = [];
    var loginObserverCallbacks              = [];
    var disableElementsCallbacks            = [];
    var machineStateObserverCallbacks       = [];
    var priveledgedObserverCallbacks        = [];
    var logOutObserverCallbacks             = [];
    var daemonRunningObserverCallBacks      = [];
    var commandOnGoingObserverCallBacks     = [];
    var webSocketConnectedObserverCallBacks = [];

    //____________________________________________________________________
    var registerPriviledgedObserverCallback = function (callback) {
      priveledgedObserverCallbacks.push(callback);
    };
    var notifyPriviledgeObserver            = function () {
      //console.log("service-->notifyObservers notifyPriviledgeObserver");
      angular.forEach(priveledgedObserverCallbacks, function (callback) {
        var notification = {
          message: "Priviledge Changed or escalation in progress",
          view: "State Machine"
        };
        callback(notification);
      });
    };

    var registerCurrentviewObserverCallback = function (callback) {
      updateCurrentviewObserverCallbacks.push(callback);
    };
    var notifyUpdateCurrentviewObservers    = function () {
      //console.log("service-->notifyObservers updatePageNameObserverCallbacks");
      angular.forEach(updateCurrentviewObserverCallbacks, function (callback) {
        callback();
      });
    };

    var registerloginObserverCallbacks = function (callback) {
      //console.log("service-->registering login observer:"+callback);
      loginObserverCallbacks.push(callback);
    };
    var notifyloginObserver            = function () {
      //console.log("service-->notifyObservers notifyloginObserver:"+loginObserverCallbacks);
      angular.forEach(loginObserverCallbacks, function (callback) {
        callback();
      });
    };

    var registerCheckPriviledgeObserverCallbacks = function (callback) {
      //console.log("service-->registering login observer:"+callback);
      checkPriviledgeObserverCallbacks.push(callback);
    };
    var notifycheckPriviledgeObserver            = function () {
      //console.log("service-->notifyObservers notifyloginObserver:"+loginObserverCallbacks);
      angular.forEach(checkPriviledgeObserverCallbacks, function (callback) {
        callback();
      });
    };

    var registerDisableElementsCallbacks = function (callback) {
      //console.log("service-->registering disableElements observer:"+callback);
      disableElementsCallbacks.push(callback);
    };
    var notifyDisableElementsObserver    = function () {
      console.log("service-->notifyObservers disableElementsObserver:" + disableElements);
      angular.forEach(disableElementsCallbacks, function (callback) {
        //console.log("service-->notifyObservers disableElementsObserver:"+callback);
        callback();
      });
    };

    var registermachineStateObserverCallbacks = function (callback) {
      //console.log("service-->registermachineStateObserverCallbacks" + callback);
      machineStateObserverCallbacks.push(callback);
    };
    var notifymachineStateChangeObservers     = function (notification) {
      //console.log("service-->notifyObservers notifymachineStateChangeObservers");
      angular.forEach(machineStateObserverCallbacks, function (callback) {
        callback(notification);
      });
    };

    var registerLogOutObserverCallbacks = function (callback) {
      //console.log("service-->registerLogOutObserverCallbacks" + callback);
      logOutObserverCallbacks.push(callback);
    };

    var notifyLogOutObserver                        = function () {
      //console.log("service-->notifyObservers notifyLogOutChangeObservers");
      angular.forEach(logOutObserverCallbacks, function (callback) {
        callback();
      });
    };
    var registerDaemonRunningObserverCallBacks      = function (callback) {
      daemonRunningObserverCallBacks.push(callback);
    };
    var notifyDaemonRunningObserver                 = function () {
      angular.forEach(daemonRunningObserverCallBacks, function (callback) {
        $log.log("service-->notifyDaemonRunningObserver");

        callback(daemonStatus.daemonActive);
      });
    };
    var registerCommandOngoingObserverCallBacks     = function (callback) {
      commandOnGoingObserverCallBacks.push(callback);
    };
    var notifyCommandOngoingObserver                = function () {
      angular.forEach(commandOnGoingObserverCallBacks, function (callback) {
        $log.log("service-->notifyDaemonRunningObserver");

        callback(commandOnGoing);
      });
    };
    var registerWebSocketConnectedObserverCallBacks = function (callback) {
      webSocketConnectedObserverCallBacks.push(callback);
    };
    var notifyWebsocketConnectedObserver            = function () {
      angular.forEach(webSocketConnectedObserverCallBacks, function (callback) {
        $log.log("service-->notifyWebsocketConnectedObserver");
        callback(websocketConnected);
      });
    };
    //_________________________________________________________________

    var getShowToViewer = function () {
      //console.log("service-->ShowToViewer: " + showToViewer);
      return showToViewer;
    };
    var setShowToViewer = function (newShowToViewer) {
      //console.log("service-->setShowToViewer: " + newShowToViewer);
      showToViewer = newShowToViewer;
      console.log("service-->to: " + showToViewer);
    };

    var getShowToExpert = function () {
      //console.log("service-->getShowToExpert: " + showToExpert);
      return showToExpert;
    };
    var setShowToExpert = function (newShowToExpert) {
      //console.log("service-->setShowToViewer: " + newShowToExpert);
      showToExpert = newShowToExpert;
      //console.log("service-->to: " + showToExpert);
    };

    var getShowToRoot = function () {
      //console.log("service-->getShowToRoot: " + showToRoot);
      return showToRoot;
    };
    var setShowToRoot = function (newShowToRoot) {
      //console.log("service-->setShowToViewer: " + newShowToRoot);
      showToRoot = newShowToRoot;
      console.log("service-->to: " + showToRoot);
    };

    var getRole     = function () {
      //console.log("service-->getRole: " + role);
      return role;
    };
    var setRole     = function (newRole) {
      //console.log("service-->setRole: " + newRole);
      role = newRole;
      //console.log("service-->to: " + role);
    };
    var getUsername = function () {
      //console.log("service-->getUsername: " + username);
      return username;
    };
    var setUsername = function (newUsername) {
      //console.log("service-->setUsername: " + newUsername);
      username = newUsername;
      //console.log("service-->to: " + username);
    };
    var getName     = function () {
      //console.log("service-->getName: " + name);
      return name;
    };
    var setName     = function (newName) {
      //console.log("service-->setName: " + newName);
      name = newName;
      //console.log("service-->to: " + name);
    };

    var getStateMachineState = function () {
      //console.log("service-->getStateMachineState: " + stateMachineState);
      return angular.isDefined(daemonStatus.currentState) ?
        daemonStatus.currentState
        : "";
    };

    var getDisableElements = function () {
      //console.log("service-->getDisableElements: " + disableElements);
      return disableElements;
    };
    var setDisableElements = function (newDisableElements) {
      console.log("service-->setDisableElements: " + newDisableElements);
      disableElements = newDisableElements;
      //console.log("service-->to: " + disableElements);
      //maybe observer
      //console.log("service-->notifing observer setDisableElements");
      notifyDisableElementsObserver();
    };
    var setResultFromLogin = function (result) {
      setRole(result.role);
      setUsername(result.username);
      setName(result.name);
      isLogged = true;
      notifyloginObserver();
    };
    var getIsLogged        = function () {
      return isLogged;
    };
    var setlogOut          = function () {
      isLogged = false;

      notifyLogOutObserver();
    };

    var setPriviledgeStatus = function (newPriviledge) {
      //console.log("service-->setPriviledge: " + newPriviledge);
      priviledged = newPriviledge;
      //console.log("service-->to: " + priviledged);
      //maybe observer
      //console.log("service-->notifing observer setPriviledgeStatus");
      //console.log("service-->GlobalDataService.setPriviledgeStatus: notification ommited");

    };
    var getPriviledgeStatus = function () {
      //console.log("service-->getPriviledgeStatus: " + priviledged);
      return priviledged;
    };

    var setCurrentPriviledgedName     = function (newCurrentPriviledgedName) {
      //console.log("service-->setCurrentPriviledgedName: " + newCurrentPriviledgedName);
      currentPriviledgedName = newCurrentPriviledgedName;
      //console.log("service-->to: " + currentPriviledgedName);
    };
    var getCurrentPriviledgedName     = function () {
      //console.log("service-->getCurrentPriviledgedName: " + currentPriviledgedName);
      return currentPriviledgedName;
    };
    var setCurrentPriviledgedUsername = function (newCurrentPriviledgedUsername) {
      //console.log("service-->setCurrentPriviledgedUsername: " + newCurrentPriviledgedUsername);
      currentPriviledgedUsername = newCurrentPriviledgedUsername;
      //console.log("service-->to: " + currentPriviledgedUsername);
    };
    var getCurrentPriviledgedUsername = function () {
      //console.log("service-->getCurrentPriviledgedUsername: " + currentPriviledgedUsername);
      return currentPriviledgedUsername;
    };

    var setCurrentEscalatingName = function (newCurrentEscalatingName) {
      //console.log("service-->newCurrentEscalatingName: " + newCurrentEscalatingName);
      currentEscalatingName = newCurrentEscalatingName;
      //console.log("service-->to: " + currentEscalatingName);
    };
    var getCurrentEscalatingName = function () {
      //console.log("service-->getCurrentEscalatingName: " + currentEscalatingName);
      return currentEscalatingName;
    };

    var setPriviledgeWillExpireInSeconds = function (newPriviledgeWillExpireInSeconds) {
      //console.log("service-->newPriviledgeWillExpireInSeconds: " + newPriviledgeWillExpireInSeconds);
      priviledgeWillExpire = newPriviledgeWillExpireInSeconds;
      //console.log("service-->to: " + priviledgeWillExpire);
    };
    var setAutograntWillExpireInSeconds = function (newAutograntWillExpire) {
      //console.log("service-->newPriviledgeWillExpireInSeconds: " + newPriviledgeWillExpireInSeconds);
      autograntWillExpire = newAutograntWillExpire;
      //console.log("service-->to: " + priviledgeWillExpire);
    };

    var getPriviledgeWillExpireInSeconds = function () {
      //console.log("service-->getPriviledgeWillExpire: " + priviledgeWillExpire);
      return priviledgeWillExpire;
    };
    var getAutograntWillExpireInSeconds = function () {
      //console.log("service-->getPriviledgeWillExpire: " + priviledgeWillExpire);
      return autograntWillExpire;
    };

    var getProcessesStatus = function () {
      return daemonStatus.processes;
    };

    var getDaemonRunning = function () {
      return daemonStatus.daemonActive;
    };

    var getLastcommandSuccesful = function () {
      return daemonStatus.exitStatus;
    };
    var getRunsetup             = function () {
      var tmp = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.RUNSETUP) ?
          daemonStatus.infos.RUNSETUP.toString().split("/")
          : ""
        : "";
      ret     = tmp[tmp.length - 1];
      return ret;
    };
    var getDatacard             = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.RUNDATACARD) ?
          daemonStatus.infos.RUNDATACARD.toString()
          : ""
        : "";
      return ret;
    };
    var getReason = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.reason) ?
          daemonStatus.infos.reason.toString()
          : ""
        : "";
      return ret;
    };
    var getRunnumber = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.RUNNUMBER) ?
          daemonStatus.infos.RUNNUMBER.toString()
          : ""
        : "";

      return ret;
    };
    var getIdleTime = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.idle_timestamp) ?
          daemonStatus.infos.idle_timestamp
          : 0
        : 0;
      return ret;
    };
    var getStandbyTime = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.standby_timestamp) ?
          daemonStatus.infos.standby_timestamp
          : 0
        : 0;
      return ret;
    };
    var getReadyTime = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.ready_timestamp) ?
          daemonStatus.infos.ready_timestamp
          : 0
        : 0;
      return ret;
    };
    var getRunningTime = function () {
      var ret = angular.isDefined(daemonStatus.infos) ?
        angular.isDefined(daemonStatus.infos.running_timestamp) ?
          daemonStatus.infos.running_timestamp
          : 0
        : 0;
      return ret;
    };
    var getStateTimestamp = function () {
      var ret = angular.isDefined(daemonStatus.timestamp) ?
          daemonStatus.timestamp
        : 0;
      return ret;
    };
    var getCommandOnGoing = function () {
      return daemonStatus.commandOnGoing;
    };
    var setDaemonStatus   = function (newdaemonStatus) {
      var notification = {
        message: "State Machine: " + newdaemonStatus.currentState,
        view: "State Machine",
        data: {
          statechange: daemonStatus.currentState != newdaemonStatus.currentState
        }
      };
      console.log(notification);

      daemonStatus = newdaemonStatus;

      notifyDaemonRunningObserver();
      notifyCommandOngoingObserver();

      notifymachineStateChangeObservers(notification);
    };
    var getDaemonStatus   = function () {
      return daemonStatus;
    };

    var setWebsocketConnected = function (newwebsocketConnectedValue) {
      websocketConnected = newwebsocketConnectedValue;
      notifyWebsocketConnectedObserver(websocketConnected);
    };
    var getWebsocketConnected = function () {
      return websocketConnected;
    };
    return {

      getRole: getRole,
      setRole: setRole,
      getUsername: getUsername,
      setUsername: setUsername,
      getName: getName,
      setName: setName,

      setResultFromLogin: setResultFromLogin,

      getShowToViewer: getShowToViewer,
      setShowToViewer: setShowToViewer,
      getShowToExpert: getShowToExpert,
      setShowToExpert: setShowToExpert,
      getShowToRoot: getShowToRoot,
      setShowToRoot: setShowToRoot,

      getDisableElements: getDisableElements,
      setDisableElements: setDisableElements,
      getStateMachineState: getStateMachineState,

      setCurrentPriviledgedName: setCurrentPriviledgedName,
      getCurrentPriviledgedName: getCurrentPriviledgedName,
      setCurrentPriviledgedUsername: setCurrentPriviledgedUsername,
      getCurrentPriviledgedUsername: getCurrentPriviledgedUsername,
      setCurrentEscalatingName: setCurrentEscalatingName,
      getCurrentEscalatingName: getCurrentEscalatingName,
      setPriviledgeWillExpireInSeconds: setPriviledgeWillExpireInSeconds,
      setAutograntWillExpireInSeconds: setAutograntWillExpireInSeconds,
      getPriviledgeWillExpireInSeconds: getPriviledgeWillExpireInSeconds,
      getAutograntWillExpireInSeconds: getAutograntWillExpireInSeconds,
      setPriviledgeStatus: setPriviledgeStatus,
      getPriviledgeStatus: getPriviledgeStatus,

      //setPriviledgeChange: setPriviledgeChange,
      //setPriviledge: setPriviledge,

      getProcessStatus: getProcessesStatus,
      getDaemonRunning: getDaemonRunning,
      getLastcommandSuccesful: getLastcommandSuccesful,
      getRunsetup: getRunsetup,
      getDatacard: getDatacard,
      getReason: getReason,
      getRunnumber:getRunnumber,
      getIdleTime: getIdleTime,
      getStandbyTime: getStandbyTime,
      getReadyTime: getReadyTime,
      getRunningTime: getRunningTime,
      getStateTimestamp: getStateTimestamp,
      setWebsocketConnected: setWebsocketConnected,
      getWebsocketConnected: getWebsocketConnected,
      getCommandOnGoing: getCommandOnGoing,
      setDaemonStatus: setDaemonStatus,
      getDaemonStatus: getDaemonStatus,
      setlogOut: setlogOut,
      getIsLogged: getIsLogged,

      registerCurrentviewObserver: registerCurrentviewObserverCallback,
      registerLoginObserver: registerloginObserverCallbacks,
      registerDisableElementsObserver: registerDisableElementsCallbacks,
      registermachineStateObserver: registermachineStateObserverCallbacks,
      registerPriviledgedObserver: registerPriviledgedObserverCallback,
      notifyPriviledgeObserver: notifyPriviledgeObserver,
      registerLogOutObserver: registerLogOutObserverCallbacks,
      registerDaemonRunningObserver: registerDaemonRunningObserverCallBacks,
      registerCommandOngoingObserver: registerCommandOngoingObserverCallBacks,
      registerWebSocketConnectedObserver: registerWebSocketConnectedObserverCallBacks

    };
  }]);
