/**
 *  Factory
 **/

//Factory
app.factory('RemoteApiFactory', function ($http) {
  //var remoteApiSite="proxy.php"
  var remoteApiSite = "/km3itawebserver/";

  return {
    login: function (data) {
      var serviceUrl = remoteApiSite + "login";
      console.log(serviceUrl);
      console.log(data);
      data.password = sha256_digest(data.password);

      $http.defaults.useXDomain = true;
      var config                = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    logout: function (token) {
      var serviceUrl = remoteApiSite + "logout";
      console.log(serviceUrl);
      var data = {"authToken": token};
      console.log(data);

      $http.defaults.useXDomain = true;
      var config                = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    checkToken: function (token) {
      var serviceUrl = remoteApiSite + "verifyToken";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {"authToken": token};
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    /*
     * {
     * command: "amiprivileged",
     * authToken: "[previously received token]"
     * }
     */
    amIPriviledged: function (token) {
      var serviceUrl = remoteApiSite + "escalate";
      console.log(serviceUrl);
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "amiprivileged"
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    iwouldliketoescalate: function (token, forceEscalation) {
      var serviceUrl = remoteApiSite + "escalate";
      console.log(serviceUrl);
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "forceEscalation": forceEscalation,
        "command": "iwouldliketoescalate"
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    releasePriviledge: function (token) {
      var serviceUrl = remoteApiSite + "escalate";
      console.log(serviceUrl);
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "releaseprivilege"
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    /*
     * {
     * command: "[init|configure|start|stop|reset]",
     * param: "[better explained afterwords]",
     * authToken: "[previously received token]"
     * }
     */
    initCommand: function (token, runsetup) {
      var serviceUrl = remoteApiSite + "commands";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data = {
        "authToken": token,
        "command": "init",
        "param": runsetup,
      };

      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    configureCommand: function (token) {
      var serviceUrl = remoteApiSite + "commands";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "configure",
        "param": "",
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    startCommand: function (token) {
      var serviceUrl = remoteApiSite + "commands";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "start",
        "param": "",
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    stopCommand: function (token) {
      var serviceUrl = remoteApiSite + "commands";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "stop",
        "param": "",
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    resetCommand: function (token) {
      var serviceUrl = remoteApiSite + "commands";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "reset",
        "param": "",
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    /*
     * {
     * command: "statemachine|processes",
     * param: "[better explained afterwords]",
     * authToken: "[previously received token]"
     * }
     * */
    statusCommand: function (token) {
      var serviceUrl = remoteApiSite + "status";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "status",
        "param": ""
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config); //<-----------------------------------
    },
    getPublishedRunSetupListCommand: function (token) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      var data   = {
        "authToken": token,
        "command": "getPublishedRunSetupList",
        "param": {}
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    getPublishedRunSetupJson: function (token, id) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "getPublishedRunSetup",
        "param": {
          "id": id
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    getDraftRunSetupJson: function (token, id) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "getDraftRunSetup",
        "param": {
          "id": id
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    deleteDraftRunsetup: function (token, id) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "deleteDraftRunSetup",
        "param": {
          "id": id
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    updateRunsetupDetector: function (token, id, detectorid) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      var data   = {
        "authToken": token,
        "command": "updateDraftRunsetupDetector",
        "param": {
          "id": id,
          "detectorId": detectorid
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    cloneDraftRunsetup: function (token, id) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;

      var data   = {
        "authToken": token,
        "command": "cloneDraftRunsetup",
        "param": {
          "id": id
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    updateExistingDraftRunsetup: function (token,
                              id,
                              runsetupContent, detectorId,
                              description) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "updateDraftRunsetupContent",
        "param": {
          "id": id,
          "data": runsetupContent,
          "detectorId": detectorId,
          "description": description
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);

    },
    saveNewRunsetup: function (token,
                               description,
                               runsetupcontent,
                                detectorId) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);

      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "newDraftRunsetup",
        "param": {
          "description": description,
          "data": runsetupcontent,
          "detectorId": detectorId
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);

    },

    publishDraftRunsetup: function (token, id) {
    var serviceUrl = remoteApiSite + "runsetup";
    console.log(serviceUrl);

    $http.defaults.useXDomain = true;
    var data   = {
      "authToken": token,
      "command": "publishRunsetup",
      "param": {
        "id": id
      }
    };
    var config = {
      headers: {
        //'Authorization': 'Basic YmVlcDpib29w',
        'Content-Type': 'application/json; charset=utf-8'
      },
      method: 'POST',
      url: serviceUrl,
      data: data
    };

    return $http(config);
  },

    getDetectorListCommand: function (token) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);
      $http.defaults.useXDomain = true;
      var data   = {
        "authToken": token,
        "command": "getDetectorList",
        "param": {}
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);

    },
    // not used anymore kept only for documentation
    uploadRunSetup: function (token, content, fileFD, filename) {
      var serviceUrl = remoteApiSite + "runsetup";
      console.log(serviceUrl);
      console.log(fileFD);

      var fd = new FormData();
      fd.append('file', fileFD);
      fd.append('authToken', token);
      fd.append('command', "insertRunsertup");
      //fd.append('param', JSON.stringify(param));

      //fd.append('data', JSON.stringify(data));
      console.log(fd);

      var headers = {
        withCredentials: false,
        headers: {
          'Content-Type': undefined
        },
        transformRequest: angular.identity

        //responseType: "arraybuffer"
      };

      return $http.post(serviceUrl, fd, headers);
    },

    moveRunsetupToDraftCommand: function (token, runsetupid) {
      var serviceUrl            = remoteApiSite + "runsetup";
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "movePublishedRunsetupToDraft",
        "param": {
          "id": runsetupid
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };

      return $http(config);
    },
    getRunsetupDraftListCommand: function (token) {
      var serviceUrl            = remoteApiSite + "runsetup";
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "getDraftRunSetupList",
        "param": {}
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };
      return $http(config);

    },
    ckeckAndSaveRunsetup: function (token, runsetupContent, detector) {
      var serviceUrl            = remoteApiSite + "runsetup";
      $http.defaults.useXDomain = true;

      //token is a plain string create obj for json
      var data   = {
        "authToken": token,
        "command": "checkAndSaveRunSetup",
        "param": {
          runsetupContent: runsetupContent,
          detectorContent: detector
        }
      };
      var config = {
        headers: {
          //'Authorization': 'Basic YmVlcDpib29w',
          'Content-Type': 'application/json; charset=utf-8'
        },
        method: 'POST',
        url: serviceUrl,
        data: data
      };
      return $http(config);
    }
  }
});

app.factory('focus', function ($timeout) {
  return function (id) {
    // timeout makes sure that is invoked after any other event has been triggered.
    // e.g. click events that need to run before the focus or
    // inputs elements that are in a disabled state but are enabled when those events
    // are triggered.
    $timeout(function () {
      var element = document.getElementById(id);
      if (element)
        element.focus();
      else
        console.log("nun c'è' l'element" + element);
    }, 1000);
  };
});
