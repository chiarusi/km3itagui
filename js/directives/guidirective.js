/**
 * Created by favaro on 20/01/16.
 */
app.directive('onReadFile', function ($parse) {
  return {
    restrict: 'A',
    scope: false,
    link: function (scope, element, attrs) {
      var fn = $parse(attrs.onReadFile);

      element.on('change', function (onChangeEvent) {
        var reader = new FileReader();

        reader.onload = function (onLoadEvent) {
          scope.$apply(function () {
            fn(scope, {
              $fileContent: onLoadEvent.target.result,
              $filename: onChangeEvent.target.files[0].name,
              $fileFD: onChangeEvent.target.files[0]
            });
          });
        };
        reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
      });
    }
  };
});