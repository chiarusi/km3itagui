//app config

app.config(function (uiSelectConfig) {
  uiSelectConfig.theme = 'bootstrap';
});

app.config(function ($wampProvider) {
  $wampProvider.init({
                       url: "ws://" + window.location.host.replace(/\:.*$/,
                                                                   "") + ":8080",  //'ws://km3webserversocket.localdomain:8080/',
                       realm: 'realm1',
                       //Any other AutobahnJS options
                       max_retries: -1,
                       initial_retry_delay: 10
                     });
})

app.config(function (JSONEditorProvider) {
  // these are set by default, but we set this for demonstration purposes
  JSONEditorProvider.configure({
                                 defaults: {
                                   options: {
                                     iconlib: 'bootstrap3',
                                     theme: 'bootstrap3'
                                   }
                                 }
                               });

});