'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/statemachine');

        // Application routes
        $stateProvider
            .state('Index', {
                url: '/dashboard',
                views: {
                  "content": {
                    templateUrl: 'templates/dashboard.html'
                  },
                  "sidebar": {
                    templateUrl: 'templates/sidebar.html'
                  },
                  "headerbar": {
                    templateUrl: 'templates/header.html'
                  }
                }

            })
            .state('State Machine', {
                url: '/statemachine',
                views: {
                  "content": {
                    templateUrl: 'templates/statemachine.html'
                  },
                  "sidebar": {
                    templateUrl: 'templates/sidebar.html'
                  },
                  "headerbar": {
                    templateUrl: 'templates/header.html'
                  }
                }

            })
          .state('Runsetups', {
                url: '/runsetups',
                views: {
                  "content": {
                    templateUrl: 'templates/RunSetup.html'
                  },
                  "sidebar": {
                    templateUrl: 'templates/sidebar.html'
                  },
                  "headerbar": {
                    templateUrl: 'templates/header.html'
                  }
                }

            })
          .state('Runsetup Editor', {
                url: '/runsetupeditor',
                params: {
                  runsetup: null
                },
                views: {
                  "content": {
                    templateUrl: 'templates/RunSetupEditor.html'
                  },
                  "sidebar": {
                    templateUrl: 'templates/sidebar.html'
                  },
                  "headerbar": {
                    templateUrl: 'templates/header.html'
                  }
                }

            });



    }
]);

