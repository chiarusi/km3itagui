app.filter('reverse', function () {
  return function (items) {
    return items.slice().reverse();
  };
});
app.filter('notification', function () {
  return function (items, value) {
    return items.filter(function (element) {
      return this.view != element.view;
    }, value);
  };
});

//app filter
/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
app.filter('propsFilter', function () {
  return function (items,texttofind) {
    var out = [];
    //console.log('"'+texttofind+'"');

    if (texttofind != "") {
      items.forEach(function (item) {
        //console.log(item);

        if (item.toString().toLowerCase().indexOf(texttofind.toLowerCase) !== -1) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }
    //console.log(out);
    return out;
  };
});

app.filter('secondsToDateTime', [function() {
  return function(statetimestamp, timestamp) {
    date_state = new Date().setTime(statetimestamp*1000);
    date_timestamp = new Date(timestamp*1000);

    seconds = Math.floor((date_timestamp - (date_state))/1000);
    minutes = Math.floor(seconds/60);
    hours = Math.floor(minutes/60);
    days = Math.floor(hours/24);

    hours = hours-(days*24);
    minutes = minutes-(days*24*60)-(hours*60);
    seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);

    return (days != 0? days +"d " : "") +
      (hours != 0? ("00" + hours).substr(-2,2) + "h " : "" ) +
      (minutes != 0? ("00" + minutes).substr(-2,2) + "m " : "" ) +
      (seconds != 0? ("00" + seconds).substr(-2,2) + "s" : "" ) ;

  };
}]);