angular.module('km3GuiApp', [
	'km3GuiApp.controllers','ngRoute'
]);

/*
angular.module('myApp').config(function($routeProvider){
    $routeProvider.when('/view1',{
        controller:'Controller1',
        templateUrl:'partials/view1.html'
    }).when('/view2',{
        controller: 'Controller2',
        templateUrl: 'partials/view2.html'
    });
});
*/

angular.module('km3GuiApp').config(function($routeProvider){
    $routeProvider.when('/login', {
        controller:'km3LoginController',
        templateUrl: 'partials/login.html'
    }).when('/main', {
        controller:'km3NavigationController',
        templateUrl: 'partials/main.html'
    }).when('/stateMachine', {
        controller:'km3stateMachineController',
        templateUrl: 'partials/state_machine.html'
    }).when('/loading', {
        controller:'km3GuiController',
        templateUrl: 'partials/site_loading.html'
    }).otherwise({
    	redirectTo: '/loading'
    });
});
